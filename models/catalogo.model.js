const { Schema } = require('mongoose');

const catalogoSchema = new Schema({
    id: Number,
    descripcion: String,
    codigo: String,
});

catalogoSchema.method('toJSON', function () {
    const { _id, __v, ...object } = this.toObject();
    object.id = _id
    return object;
});

module.exports = { catalogoSchema };