const { Schema, model } = require('mongoose');

const usuarioSchema = new Schema({
    id: Schema.Types.ObjectId,
    primerNombre: { required: true, type: String },
    segundoNombre: { required: true, type: String },
    primerApellido: { required: true, type: String },
    segundoApellido: { required: true, type: String },
    identificacion: { required: true, type: String },
    fechaNacimiento: { required: true, type: Date },
    edad: { required: true, type: Number },
    telefono: { type: Number },
    celular: { type: Number },
    email: { type: String },
    tieneDatosAdicionales: { type: Boolean },
    tipoDeIdentificacion: {
        type: Schema.Types.ObjectId,
        ref: 'TipoIdentificacion',
        required: true
    },
    genero: {
        type: Schema.Types.ObjectId,
        ref: 'Genero',
        required: true
    },
    tipoSangre: {
        type: Schema.Types.ObjectId,
        ref: 'TipoSangre',
        required: true
    },
    FactorRH: {
        type: Schema.Types.ObjectId,
        ref: 'FactorRH',
        required: true
    },

});

usuarioSchema.method('toJSON', function () {
    const { _id, __v, ...object } = this.toObject();
    object.id = _id;
    return object;
})

module.exports = model('Usuario', usuarioSchema);