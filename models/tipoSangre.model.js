const { catalogoSchema } = require('./catalogo.model');
const { model } = require('mongoose');

module.exports = model('TipoSangre', catalogoSchema)