const { catalogoSchema } = require('./catalogo.model');
const { model } = require('mongoose');

module.exports = model('TipoIdentificacion', catalogoSchema)
