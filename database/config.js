const { connect } = require('mongoose');

const conectarDB = async () => {
    try {
        await connect(process.env.DB_CNN,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useCreateIndex: true,
                useFindAndModify: false
            })

    console.log('BD ON');
    } catch (error) {
        console.log('Error al conectar con la BD');
        console.log(error);
    }
}

module.exports = { conectarDB }
