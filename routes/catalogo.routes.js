const { Router } = require('express')
const { getCatalogo, postCatalogo } = require('./../controllers/catalogo.controller')

const router = Router();

router.get('/:nombreCatalogo', getCatalogo);
router.post('', postCatalogo);

module.exports = router;
