const { Router } = require('express');
const { check } = require('express-validator');
const { getUsuario, postUsuario, putUsuario, deleteUsuario } = require('./../controllers/usuario.controller');
const { validarCampos } = require('./../middleware/validarCampos');

const router = Router();


router.get('', getUsuario);
router.post('', [
    check('primerApellido', 'El primerApellido del usuario es obligatorio').not().isEmpty(),
    check('segundoApellido', 'El segundoApellido del usuario es obligatorio').not().isEmpty(),
    check('primerNombre', 'El primerNombre del usuario es obligatorio').not().isEmpty(),
    check('segundoNombre', 'El segundoNombre del usuario es obligatorio').not().isEmpty(),
    check('tipoDeIdentificacion', 'Debe ser un id de mongo valido').isMongoId(),
    check('identificacion', 'El identificacion del usuario es obligatorio').not().isEmpty(),
    check('tipoSangre', 'Debe ser un id de mongo valido').isMongoId(),
    check('FactorRH', 'Debe ser un id de mongo valido').isMongoId(),
    check('genero', 'Debe ser un id de mongo valido').isMongoId(),
    check('edad', 'El edad del usuario es obligatorio').not().isEmpty(),
    check('fechaNacimiento', 'El fechaNacimiento del usuario es obligatorio').not().isEmpty(),
    check('tieneDatosAdicionales', 'El tieneDatosAdicionales del usuario es obligatorio').not().isEmpty(),
    validarCampos
], postUsuario);
router.put('/:id', putUsuario);
router.delete('/:id', deleteUsuario);

module.exports = router;
