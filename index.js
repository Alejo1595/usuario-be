require('dotenv').config();
const express = require('express');
const cors = require('cors');

const { conectarDB } = require('./database/config');

const app = express();

app.use(cors());

app.use(express.json());

conectarDB();

app.use(express.static('public'));

app.use('/api/usuarios', require('./routes/usuario.routes'));
app.use('/api/catalogo', require('./routes/catalogo.routes'));


app.listen(process.env.PORT, () => console.log(`Escuchando en el puerto ${process.env.PORT}`))
