const { request, response } = require('express');
const CatTipoIdentificacion = require('./../models/tipoIdentificacion.model');
const CatGenero = require('./../models/genero.model');
const CatTipoSangre = require('./../models/tipoSangre.model');
const CatFactorRH = require('./../models/factorRH.model');


const getCatalogo = async (req = request, res = response) => {
    try {
        const { nombreCatalogo } = req.params;
        let catalogoDB;
        switch (nombreCatalogo) {
            case 'tipoIdentificacion':
                catalogoDB = await CatTipoIdentificacion.find();
                break;
            case 'genero':
                catalogoDB = await CatGenero.find();
                break;
            case 'tipoSangre':
                catalogoDB = await CatTipoSangre.find();
                break;
            case 'factorRH':
                catalogoDB = await CatFactorRH.find();
                break;
            default:
                return res.status(404).json({ mensaje: 'Catalogo no existe' })
        }
        res.status(200).json(catalogoDB)
    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: 'Error al obtener un catalogo, revisar log' });
    }
}

const postCatalogo = async (req = request, res = response) => {
    try {
        const { descripcion, codigo } = req.body;
        const catalogoDB = new CatFactorRH({ descripcion, codigo });
        const catalogo = await catalogoDB.save();
        res.status(200).json({ ok: true, catalogo })

    } catch (error) {
        console.log(error);
        res.status(500).json({ ok: false, msg: 'Error inesperado, revisar log' })
    }
}


module.exports = { postCatalogo, getCatalogo };
