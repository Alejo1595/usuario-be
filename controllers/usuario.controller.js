const { request, response } = require('express');
const Usuario = require('./../models/usuario.model');


const postUsuario = async (req = request, res = response) => {
    try {
        const usuarioDB = new Usuario(req.body);
        await usuarioDB.save();
        res.status(200).json({ mensaje: 'Creación exitosa' })

    } catch (error) {
        console.log(error);
        res.status(500).json({ mensaje: 'Error inesperado, contacte con el administrador' })
    }
}

const getUsuario = async (req = request, res = response) => {
    try {
        const listaUsuariosDB = await Usuario.find()
            .populate('tipoDeIdentificacion', 'descripcion')
            .populate('genero', 'descripcion')
            .populate('tipoSangre', 'descripcion')
            .populate('FactorRH', 'descripcion')

        const listaUsuarios = crearCuerpoUsuarios(listaUsuariosDB);
        res.status(200).json(listaUsuarios);
    } catch (error) {
        console.log(error);
        res.status(500).json({ mensaje: 'Error inesperado, contacte con el administrador' });
    }
}

const putUsuario = async (req = request, res = response) => {
    try {
        const { id } = req.params;
        const existeUsuario = await Usuario.findById(id);
        if (!existeUsuario) {
            return res.status(401).json({ mensaje: `No existe un usuario con el id: ${id}` });
        }

        await Usuario.findByIdAndUpdate(id, req.body, { new: true });
        res.status(200).json({ mensaje: 'Actualizacion exitosa' });
    } catch (error) {
        console.log(error);
        res.status(500).json({ mensaje: 'Error inesperado, contacte con el administrador' });
    }
}

const deleteUsuario = async (req = request, res = response) => {
    try {
        const listaId = req.params.id.split(',');
        let respuesta;
        let idActual;
        for (const id of listaId) {
            idActual = id
            respuesta = await determinarSiPuedeEliminar(id);
            if (respuesta) {
                await Usuario.findByIdAndDelete(id);
            }
        }
        (respuesta)
            ? res.status(200).json({ mensaje: 'Eliminación exitosa' })
            : res.status(401).json({ mensaje: `No existe un usuario con el id: ${idActual}` });
    } catch (error) {
        console.log(error);
        res.status(500).json({ mensaje: 'Error inesperado, contacte con el administrador' });
    }
}

const determinarSiPuedeEliminar = async (id) => {
    const usuarioDB = await Usuario.findById(id);
    if (!usuarioDB) {
        return false;
    }
    return true;
}

module.exports = { postUsuario, getUsuario, putUsuario, deleteUsuario };

function crearCuerpoUsuarios(listaUsuariosDB) {
    const resultado = listaUsuariosDB.map(usuario => {
        return {
            ...usuario.toJSON(),
            idTipoDeIdentificacion: usuario.tipoDeIdentificacion._id,
            idGenero: usuario.genero._id,
            idTipoSangre: usuario.tipoSangre._id,
            idFactorRH: usuario.FactorRH._id,
            desTipoDeIdentificacion: usuario.tipoDeIdentificacion.descripcion,
            descGenero: usuario.genero.descripcion,
            descTipoSangre: usuario.tipoSangre.descripcion,
            descFactorRH: usuario.FactorRH.descripcion,
            telefono: (usuario.telefono) ? usuario.telefono: null,
            celular: (usuario.celular) ? usuario.celular: null,
            email: (usuario.email) ? usuario.email: null,
        };
    });

    const listaUsuarios = eliminarCamposUsuario(resultado);
    return listaUsuarios;
}

function eliminarCamposUsuario(resultado) {
    return resultado.map(usuario => {
        const { tipoDeIdentificacion, genero, tipoSangre, FactorRH, ...listaUsuarios } = usuario;
        return listaUsuarios;
    });
}
